<?php

include 'db-connexion.php';
$statusMsg = '';
// File upload path
$targetDir = "../sources/";
$fileName = basename($_FILES["file"]["name"]);
$targetFilePath = $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
// Allow certain file formats
$allowTypes = array('jpg', 'png', 'jpeg', 'gif', 'pdf');
if (in_array($fileType, $allowTypes)) {
      // Upload file to server
      if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)) {
            // Insert image file name into database
            $description = $_POST['description'];
            $titre = $_POST['titre'];
            $categorie = $_POST['categorie'];
            $prix = $_POST['prix'];
            $requetePreparee = $dbh->prepare("
          INSERT INTO `produit` (
          `id_produit`,
          `titre_produit`,
          `prix_produit`,
          `img_produit`,
          `description_produit`,
          `id_categorie_produit`)
    
          VALUES (
          NULL,
          :titre,
          :prix,
          :lien,
          :description,
          :categorie)
          ");

            $requetePreparee->bindParam(':titre', $titre);
            $requetePreparee->bindParam(':prix', $prix);
            $requetePreparee->bindParam(':description', $description);
            $requetePreparee->bindParam(':categorie', $categorie);
            $requetePreparee->bindParam(':lien', $targetFilePath);

            $requetePreparee->execute();

            header('Location: administration.php');
            exit();
      } else {
            $statusMsg = "Sorry, there was an error uploading your file.";
      }
} else {
      $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
}

// Display status message
echo $statusMsg;
