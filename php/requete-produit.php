﻿<?php

$id = $_GET['view'];
try {

  $requetePreparee = $dbh->prepare("
    SELECT
      *
    FROM
      produit
    WHERE
      id_produit = :id
    ");

  $requetePreparee->bindParam(':id', $id);
  $requetePreparee->execute();
  $resultats = $requetePreparee->fetchAll();

  foreach ($resultats as $value) {
    require '../views/main-produit.view.phtml';
  }
} catch (PDOException $e) {

  echo "Erreur lors de l'éxécution d'une requête SQL :";

  $errorInfo = $requetePreparee->errorInfo();

  require '../views/messages-erreurs.view.phtml';
}
