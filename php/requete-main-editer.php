<?php

$id = $_GET['editer-produit'];
$requetePreparee = $dbh->prepare("
SELECT
  *
FROM
  produit
WHERE
  id_produit = :id
");

$requetePreparee->bindParam(':id', $id);
$requetePreparee->execute();
$resultats = $requetePreparee->fetchAll();

foreach ($resultats as $value) {
  require '../views/main-editer.view.phtml';
}
