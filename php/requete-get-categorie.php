<?php

$requetePreparee = $dbh->prepare("
  SELECT
  *
  FROM categorie
  ");
$requetePreparee->execute();
$categories = $requetePreparee->fetchAll();

foreach ($categories as $categorie) {

  $choice = "";
  if ($categorie['id_categorie'] == $value['id_categorie_produit'])
    $choice =  "selected";
  require '../views/main-get-categorie.view.phtml';
}
