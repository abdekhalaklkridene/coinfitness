<?php

include 'db-connexion.php';
if (isset($_SESSION['auth'])) {
  header('Location: ./compte.php');
}
if (!empty($_POST) && !empty($_POST['user-name']) && !empty($_POST['user_password'])) {
  $user_name = $_POST['user-name'];
  $password = $_POST['user_password'];
  $req = $dbh->prepare('
    SELECT
      *
    FROM
      client
        ');
  $req->bindParam(':user_name', $user_name);
  $req->execute();
  $user = $req->fetch();
  if (password_verify($password, $user['mdp_client'])) {
    $_SESSION['auth'] = $user;
    header("Location: ./administration.php");
  } else {
    die('Identifiant ou mot de passe incorrect');
  }
}
