<?php

try {

  $requetePreparee = $dbh->prepare("
    SELECT
      *
    FROM
      produit
    WHERE
      id_categorie_produit = 3
    ");

  $requetePreparee->execute();
  $resultats = $requetePreparee->fetchAll();

  foreach ($resultats as $value) {
    require '../views/administration-accessoire.view.phtml';
  }
} catch (PDOException $e) {

  echo "Erreur lors de l'éxécution d'une requête SQL :";

  $errorInfo = $requetePreparee->errorInfo();

  require '../views/messages-erreurs.view.phtml';
}
