<?php

function debug($variable)
{
  echo  "" . print_r($variable, true) . "";
}

function str_random($length)
{
  $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
  return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
}
