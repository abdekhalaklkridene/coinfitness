<?php

include 'db-connexion.php';
$delete = $_GET['supprimer-produit'];
$requetePrepare = $dbh->prepare("
  DELETE FROM `produit` WHERE `produit`.`id_produit` = :delete ;");
$requetePrepare->bindParam(':delete', $delete);
$requetePrepare->execute();

header('Location: administration.php');
exit();
