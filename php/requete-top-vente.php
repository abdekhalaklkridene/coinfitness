<?php

try {

    $requetePreparee = $dbh->prepare("
  SELECT *
   FROM produit
   WHERE prix_produit < 30 
 
    ");

    $requetePreparee->execute();
    $resultats = $requetePreparee->fetchAll();

    foreach ($resultats as $value) {
        require '../views/top-vente.view.phtml';
    }
} catch (PDOException $e) {

    echo "Erreur lors de l'éxécution d'une requête SQL :";

    $errorInfo = $requetePreparee->errorInfo();

    require '../views/messages-erreurs.view.phtml';
}
