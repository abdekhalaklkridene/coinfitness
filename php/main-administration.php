<?php

if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
if (!isset($_SESSION['auth'])) {
  echo "Vous n'avez pas l'autorisation d'accéder à cette page";
  die();
}
if ($_SESSION['auth']['username_client'] != "admin") {
  echo "Vous n'avez pas l'autorisation d'accéder à cette page";
  die();
}
include '../views/administration.view.phtml';
