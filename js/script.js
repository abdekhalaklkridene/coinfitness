var images = [
  "../images/1.jpg",
  "../images/2.jpg",
  "../images/3.jpg",
  "../images/4.jpg",
  "../images/5.jpg",
  "../images/6.jpg",

];
var boutonPlay = document.querySelector('#play');
var boutonSuivant = document.querySelector('.next');
var boutonPrecedent = document.querySelector('.prev');
var boutonAleatoire = document.querySelector('#random');
var idInterval;
var defile = false;
var indexImage=0;

// to avoid error message : Cannot read property 'addEventListener' of null when not in index

if (boutonSuivant){
  boutonSuivant.addEventListener('click', imageSuivante);
}

if (boutonPrecedent){
  boutonPrecedent.addEventListener('click', imagePrecedente);
}

if (boutonPlay){
  boutonPlay.addEventListener('click', play);
}

if (boutonAleatoire){
  boutonAleatoire.addEventListener('click', random);
}

if (document){
  document.addEventListener('keydown', clavier);
}

