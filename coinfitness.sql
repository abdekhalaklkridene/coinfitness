-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 11 déc. 2018 à 19:50
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `epiz_27863384_coinfitness`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_categorie` varchar(200) NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `nom_categorie`) VALUES
(1, 'Musculation'),
(2, 'Fitness'),
(3, 'Accessoires');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mail_client` varchar(1000) NOT NULL,
  `mdp_client` varchar(100) NOT NULL,
  `username_client` varchar(255) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf32;

INSERT INTO `client` (`id_client`, `mail_client`, `mdp_client`, `username_client`) VALUES
-- 
-- mot de passe : 'admin'
-- 
(10, 'admin@gmail.com','$2y$10$fcrIMuBoMRGNnLko2yAkJe37Q5KJF3/.A8zYPfhX9fymyeWkMFzd.','admin');

-- --------------------------------------------------------
--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id_produit` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre_produit` varchar(200) NOT NULL,
  `prix_produit` decimal(10,0) NOT NULL,
  `img_produit` text NOT NULL,
  `description_produit` text NOT NULL,
  `id_categorie_produit` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_produit`),
  KEY `id_categorie_produit` (`id_categorie_produit`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf32;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `titre_produit`, `prix_produit`, `img_produit`, `description_produit`, `id_categorie_produit`) VALUES
(1, 'Fat Gripz', '20', '../sources/fatgripz.jpg', 'Longueur : 12,7 cm - Epaisseur : 5.7cm, adaptable à toutes les barres\r\nBiseautées pour un meilleur maintien\r\nConvient aux haltères et à tout type de barres\r\nDouble l`épaisseur d`une barre olympique\r\nDimension (L x L x H ) : 6,35 x 12,7 x 6,35 cm\r\nPoids: 460 g.', 3),
(2, 'Élastique d`entrainement', '20', '../sources/elastique.jpg', 'Cette bande de résistance est parfait pour la musculation, la tonification, le crossfit, le pull-up, l`aérobic, la musculation, la perte de poids et la physiothérapie. Aussi comme un groupe de gymnastique pour l`entraînement d`échauffement ou d`étirement après une séance d`entraînement', 3),
(3, 'KETTLEBELL 8 KG', '20', '../sources/kettlebell.jpg', 'Couvercle de protection : 80.0% Caoutchouc - Styrène-butadiène, Couvercle de protection : 20.0% Caoutchouc - Butadiène Leash : 100.0% Caoutchouc thermoplastique', 1),
(4, 'Barre incurvée', '50', '../sources/barre.jpg', 'La Barre de Curl (ou barre courbée) olympique super CAP Barbell de 2 pouces, Barre de 48 pouces', 1),
(5, 'Barre triceps', '50', '../sources/barre2.jpg', 'Conçue spécifiquement pour localiser au maximum le travail des triceps avec bague en cuivre.\r\n', 1),
(6, 'Corde', '15', '../sources/corde.jpg', 'Bonne corde à sauter en groupe, adaptée à de nombreuses personnes sautant et divertissant.\r\n', 3),
(7, 'Haltéres', '35', '../sources/halteres.jpg', 'Conçus pour le renforcement musculaire et la rééducation, les haltères Vinyle permettent de travailler toute la partie supérieur du corps.\r\n', 1),
(8, 'force Poignet', '20', '../sources/main.jpg', 'ce pince musculation est conçu pour renforcer la force de poignet, de bras, les doigts, à la fois il soulage la fatigue, améliore la circulation sanguine. Avec une utilisation persistante, il prévient l`arthrite rhumatisante..\r\n', 1),
(9, 'Pédale', '60', '../sources/jambes.jpg', 'Vélo silencieux pliable: il s`agit d`une machine d`exercice à pédale MINI. Chaque fois qu`il est plié et déplié, il peut être utilisé pour l`exercice des jambes et des parties du corps.\r\n', 2),
(10, 'Barre 2 metres ', '65', '../sources/barre_2m.jpg', 'La Barre super CAP Barbell de 2 pouces, Barre de 2 métres.\r\n', 1),
(11, 'Pédale avant bras ', '35', '../sources/avant-bras.jpg', 'cette poignée d`étriers pour les exercices des pieds vous permet de tonifier les muscles des bras, des épaules, de la poitrine, des jambes.\r\n', 2),
(12, 'Ballon ', '30', '../sources/ballon.jpg', 'softball bleu clair diamètre 220mm / diamètre bleu foncé diamètre 260mm.\r\n', 2),
(13, 'Trampo ', '149', '../sources/jumper.jpg', 'trampoline de fitness fit trampo500.\r\n', 2),
(14, 'Roulette ', '35', '../sources/roulette.jpg', 'roue abdominale ab wheel.\r\n', 2),
(15, 'Tapis ', '10', '../sources/tapis.jpg', 'tapis de sol maxi grip fitness imprime 170cmx62cmx8mm rose.\r\n', 2),
(16, 'Support Pompe ', '20', '../sources/pompe.jpg', 'Support en tube d`acier pur . Support en I en forme de béquille selon la conception mécanique, stable et équilibré, difficile à déformer, adapté à l`exercice du muscle deltoïde, du biceps, du triceps.\r\n', 3),
(17, 'Gant de Musculation ', '15', '../sources/gant-musculation.jpg', 'Gant léger et adapté aux activités de plein air, et convient parfaitement à toute activité quotidienne. Des gants de haute qualité vous permettent de profiter de grandes activités de plein air. \r\n', 3);

--
-- Contraintes pour les tables déchargées
--


--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_categorie` FOREIGN KEY (`id_categorie_produit`) REFERENCES `categorie` (`id_categorie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
